package com.smartinsoft.harfbul;

import java.util.Random;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class HarfBul extends Activity{
Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13,btn14,btn15,btn16,gln,time;
private Handler hd,hd1;
boolean tb=true,tb1=true;
int bubitti=1;
int count,sh,gecici=0,l;
int b;
int score,basti;
final String[] harf={"A","B","C","�","D","E","F","G","H","I","�","J","K","L","M","N","O","�","P","R","S","�","T","U",
		"�","V","Y","Z","X","W","Q"};
final int[] dizi=new int[4];
int screenWidth,screenHeight;
private InterstitialAd interstitial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_harf_bul);
		final AdRequest adRequest = new AdRequest.Builder().build(); 
	    interstitial = new InterstitialAd(HarfBul.this);
		interstitial.setAdUnitId("ca-app-pub-1312048647642571/3018672641");
 		interstitial.loadAd(adRequest);
		interstitial.setAdListener(new AdListener() {
			public void onAdLoaded() {
			}
		});
		hd = new Handler();
		hd1 = new Handler();
		Random rk = new Random();
        int randa;
        randa = rk.nextInt(30);
        harf[0]=harf[randa];
        for(int i=1;i<4;i++)
		{
			randa = rk.nextInt(30);
		for(int j=0;j<i;j++)
		 {
			if(harf[j]==harf[randa])
			{
				i--;
			break;
			}
			else{
		  harf[i]=harf[randa];
			}
		 }
		
		}
		btn1 =(Button)findViewById(R.id.btn1);
		btn2 =(Button)findViewById(R.id.btn2);
		btn3 =(Button)findViewById(R.id.btn3);
		btn4 =(Button)findViewById(R.id.btn4);
		gln =(Button)findViewById(R.id.gln);
		time =(Button)findViewById(R.id.time);
		btn1.setText(harf[0]);
		btn2.setText(harf[1]);
		btn3.setText(harf[2]);
		btn4.setText(harf[3]);
		btn1.setTag(harf[0]);
		btn2.setTag(harf[1]);
		btn3.setTag(harf[2]);
		btn4.setTag(harf[3]);
		count=10;
		sh=10;     
        btn1.setVisibility(View.GONE);
        btn2.setVisibility(View.GONE);
        btn3.setVisibility(View.GONE);
        btn4.setVisibility(View.GONE);
        gln.setText("?");
        time.setText(String.valueOf(10));	
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		LinearLayout ly =(LinearLayout)findViewById(R.id.harfi);
		ly.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
			    float x =event.getX();
			    float y=event.getY();
			    if(screenWidth/x<1.17&&screenWidth/x>1.04&&12<screenHeight/y)
			    {
						final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
						try {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
						} catch (android.content.ActivityNotFoundException anfe) {
						    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
						}
			    }
				return true;
			}
		});
  		final Dialog dialog = new Dialog(HarfBul.this ,R.style.FullHeightDialog);
		dialog.setContentView(R.layout.custom);
		dialog.setCancelable(true);
	    final RatingBar rb = (RatingBar)dialog.findViewById(R.id.Rat);
	    Button   home = (Button)dialog.findViewById(R.id.home);
	    final Button   go = (Button)dialog.findViewById(R.id.go);
	    final TextView text = (TextView)dialog.findViewById(R.id.text);
	    home.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent Mx = new Intent(HarfBul.this,MainActivity.class);
				startActivity(Mx);
			}
		});
final Thread t = new Thread(new Runnable() {     
            
            public void run(){  
           
                  while (tb==true){
                 
                        try{
                              Thread.sleep(1000);                                                                                        
                        }
                        catch(InterruptedException e)
                        { }
                                         
                        hd.post(new Runnable(){                        
                                                     
                              public void run(){  
                            	  if(count>=0)
                             	 { 
                            		 time.setText(String.valueOf(count));
                            		 
                            		 if(count==8)
                            		 {
                            			 gln.setText(harf[dizi[0]]);
                            			 kontrol(0);
                            		 }
                            		 if(count==6)
                            		 {   basmadi();
                            			 gln.setText(harf[dizi[1]]);    
                            			 kontrol(1);
                            		 }
                            		 if(count==4)
                            		 {   basmadi();
                            			 gln.setText(harf[dizi[2]]);  
                            			 kontrol(2);
                            		 }
                            		 if(count==2)
                            		 {   basmadi();
                            			 gln.setText(harf[dizi[3]]);  
                            			 kontrol(3);
                            		 }
                            		
                            		 if(count==0)
                            		 {
                            			 basmadi();
                            		 }
                            		 
                             	}
                            	  else{
                            		  tb=false;
                            		  if(score>3)
                            		  {
                        	           text.setText("Very Good");
                        	   	    go.setOnClickListener(new View.OnClickListener() {
                        				
                        				@Override
                        				public void onClick(View v) {
                        					Intent Mx = new Intent(HarfBul.this,HarfBul2.class);
                        					startActivity(Mx);
                        				}
                        			});
                            		  }
                            		  if(score==3)
                            		  {
                        	           text.setText("Good");
                        	   	    go.setOnClickListener(new View.OnClickListener() {
                        				
                        				@Override
                        				public void onClick(View v) {
                        					Intent Mx = new Intent(HarfBul.this,HarfBul2.class);
                        					startActivity(Mx);
                        				}
                        			});
                            		  }
                            		  if(score<3)
                            		  {
                        	           text.setText("Bad!");
                        	   	       go.setVisibility(View.GONE);
                            		  }
                            		  rb.setRating((float)(score*25)*5/100);
                              		dialog.show();
                              		if (interstitial.isLoaded()) {
                              			interstitial.show();
                              		}
                            	  }
                            	  count--;
                              }
                        });
                         if(tb==false)
                         {
                         }
                  } // while
            }
        });
final Thread t1 = new Thread(new Runnable() {     
            
            public void run(){  
           
                  while (tb1==true){
                 
                        try{
                              Thread.sleep(150);                                                                                        
                        }
                        catch(InterruptedException e)
                        { }
                                         
                        hd1.post(new Runnable(){                        
                                                     
                              public void run(){  
                            	  if(sh>=0)
                             	 { 
                            		  if(bubitti==1)
                            		  {
                            				Random rt = new Random();
                                	       	gecici=rt.nextInt(4);
                                	       	dizi[0]=gecici;
                                	       	for(l=1;l<4;l++)
                                	       	{   gecici=rt.nextInt(4);
                                	       		for(int k=0;k<l;k++)
                                	       		{
                                	       			if(dizi[k]==gecici)
                                	       			{
                                	       				l--;
                                	       				break;
                                	       			}
                                	       			else{
                                	       				dizi[l]=gecici;
                                	       			}
                                	       		}
                                	       	}
                            		  }
                            		 
                            		 if(sh==8)
                            		 {
                            			 btn4.setVisibility(View.VISIBLE);
                            		 }
                            		 if(sh==6)
                            		 {
                            			 btn3.setVisibility(View.VISIBLE);
                            		 }
                            		 if(sh==4)
                            		 {
                            			 btn2.setVisibility(View.VISIBLE);
                            		 }
                            		 if(sh==2)
                            		 {
                            			 btn1.setVisibility(View.VISIBLE);
                            		 }
                             	}
                            	  else{
                            		  tb1=false;
                            	  }
                            	  sh--;
                              }
                        });
                         if(tb1==false)
                         {
                   		  t.start(); 	
                         }
                  } // while
            }
        });
t1.start();
		btn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(b==1)
				{btn1.setVisibility(View.GONE);
				score++;
				basti=1;
				}
			}
		});
		btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(b==2)
				{
				btn2.setVisibility(View.GONE);
				score++;
				basti=1;
				}
			}
		});
		btn3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(b==3)
				{
					btn3.setVisibility(View.GONE);	
					score++;
					basti=1;
				}
			}
		});
		btn4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(b==4)
				{	
				btn4.setVisibility(View.GONE);	
				score++;
				basti=1;
				}
			}
		});
	}
	public void kontrol(int i)
	{
		 if(harf[dizi[i]]==btn1.getTag())
		 {
			 b=1;
		 }
		 if(harf[dizi[i]]==btn2.getTag())
		 {
			 b=2;
		 }
		 if(harf[dizi[i]]==btn3.getTag())
		 {
			 b=3;
		 }
		 if(harf[dizi[i]]==btn4.getTag())
		 {
			 b=4;
		 }
	}
public void basmadi()
{
	 if(basti==0)
	 {
		if(b==1)
		{
			btn1.setBackgroundResource(R.drawable.kbox);
		}
		if(b==2)
		{
			btn2.setBackgroundResource(R.drawable.kbox);
		}
		if(b==3)
		{
			btn3.setBackgroundResource(R.drawable.kbox);
		}
		if(b==4)
		{
			btn4.setBackgroundResource(R.drawable.kbox);
		}
	 }
		basti=0;
}
}
