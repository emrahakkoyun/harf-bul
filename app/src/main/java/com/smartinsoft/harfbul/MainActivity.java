package com.smartinsoft.harfbul;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {
Button h1,h2,h3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        h1 = (Button)findViewById(R.id.h1);
        h2 = (Button)findViewById(R.id.h2);
        h3 = (Button)findViewById(R.id.h3);
        final Intent ih1 = new Intent(MainActivity.this,HarfBul.class);
        final Intent ih2 = new Intent(MainActivity.this,Help.class);
        h1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(ih1);
			}
		});
        h2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(ih2);
			}
		});
        h3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
    }
}
